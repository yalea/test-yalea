# Yalea test

Proceso de seleci�n en Yalea

## Getting Started

El objetivo es encontrar un curso conveniente para realizar un viaje ling��stico, para esto, se realizar� una consulta a los datos proporcionados en el archivo datos.json. 
Para saber cu�l curso es el m�s conveniente, es necesario conocer:

* N�mero de semanas
* Tipo de curso
* Escuela
* Pa�s y/o ciudad

La �nica decisi�n a tomar es que el precio por semana sea el menor, teniendo recursos y tiempo ilimitados.
Por ello es necesario que nos env�es cual es lugar y cuantas semanas se debe estar para acceder al precio m�s conveniente por semana.
Por ejemplo: si 4 semanas vale $400 y 10 semanas cuestan $900 el precio por semana m�s bajo seria $90, con lo cual nuestro curso m�s conveniente ser�a el de 10 semanas.


### Installing

Primer Clonar Proyecto

```shell
git clone git@bitbucket.org:yalea/test-yalea.git
```

una vez clonado este proyecto, asegurarse de tener instalado django 1.11 usando python 2.7.

```shell

pip install -r requirements.txt

python manage.py migrate
python manage.py loaddata datos.json

python manage.py shell
```

asegurarse que cargen los datos

```python
from yalea.models import *
Country.objects.all()
```


Se Solicita un c�digo que busque y obtenga los cursos m�s ec�nomicos dado el precio por semana.








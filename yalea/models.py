from django.db.models import ForeignKey, FloatField, IntegerField
from django.db.models import Model, TextField


class Country(Model):
    name = TextField("Country name")

    def __str__(self):
        return self.name


class City(Model):
    country = ForeignKey("Country")
    name = TextField("City name", blank=True)

    def __str__(self):
        return "{0.name}".format(self)

class LanguageSchool(Model):
    city = ForeignKey("City")
    name = TextField("School name", blank=True)
    language = TextField("Language", blank=True)

    def __str__(self):
        return "{0.city}, {0.name} ({0.language})".format(self)

class SchoolCourse(Model):
    school = ForeignKey("LanguageSchool")
    name = TextField("School name", blank=True)

    def __str__(self):
        return "{0.school}, {0.name}".format(self)

class CoursePrice(Model):
    course = ForeignKey("SchoolCourse")
    week = IntegerField("Week", blank=True)
    price = FloatField("Price", blank=True)

    def __str__(self):
        return "{0.course}: {0.week}->{0.price}->".format(self)